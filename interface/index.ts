export interface IEventRequest {
    name: string;
    phoneNumber: string;
    hopeGift: string;
}